<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	'NAME' => 'Комментарии',
	'DESCRIPTION' => 'Древовидные комментарии',
	'ICON' => '/images/comments.gif',
	'PATH' => array(
		'ID' => 'communication',
		'CHILD' => array(
			'ID' => 'comments',
			'NAME' => 'Комментарии'
		)
	),
);
?>
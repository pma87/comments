<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	'PARAMETERS' => array(
		'OBJECT_TYPE' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Тип',
			'TYPE' => 'LIST',
			'VALUES' => array(
				'IBLOCK' => 'Элемент инфоблока',
				'PAGE' => 'Страница'
			),
			'DEFAULT' => '',
		),
		'OBJECT' => array(
			'PARENT' => 'BASE',
			'NAME' => 'Объект комментирования',
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		),
		'HL_ID' => array(
			'PARENT' => 'DATA_SOURCE',
			'NAME' => 'ID Highload-блока',
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		)
	),
);

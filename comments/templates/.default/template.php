<?php
	// Подключение скриптов
	$this->addExternalJS('https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js');
	$this->addExternalJS('https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-resource.min.js');
	$this->addExternalJS('https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-sanitize.min.js');

	// Настройки для JS
	$settings = array(
		'commentObjectType' => $arParams['OBJECT_TYPE'],
		'commentObject' => $arParams['OBJECT'],
		'commentHlId' => $arParams['HL_ID'],
		'ajaxPath' => $arResult['AJAX_PATH'],
	);
?>
<script type="text/javascript">
	let pmAjaxPath = '<?=$arResult['AJAX_PATH'];?>';
</script>
<div class="pm-comments" ng-app="pmComments" ng-controller="CommentsCtrl" ng-init='init(<?=json_encode($settings);?>, <?=json_encode($arResult["DATA"]);?>)'>
	<h2>Комментарии</h2>

	<div ng-include src="'/comments.html'">
		<?if(is_array($arResult['DATA']) && count($arResult['DATA']) > 0){?>
				<?foreach($arResult['DATA'] as $k => $comment){?>
					<div class="panel" style="margin-left:<?=($comment['level'] * 50);?>px;">
						<a name="comment<?=$comment['id'];?>"></a>
						<div class="panel-body">
							<div class="panel-title pull-left">
								#<?=$comment['id'];?> <strong><?=$comment['userName'];?></strong>
								<small class="text-muted"><?=$comment['date'];?></small>
								<?if($comment['isMyComment'] && !$comment['childsNum']){?>
									<a href="?method=delete&id=<?=$comment['id'];?>#comment<?=$arResult['DATA'][$k-1]['id'];?>" class="fa fa-trash"></a>
								<?}?>
							</div>

							<a href="?parentId=<?=$comment['id'];?>#addComment" class="btn btn-xs btn-themes btn-default pull-right">Ответить</a>

							<div class="clearfix"></div>

							<p><?=$comment['text'];?></p>
						</div>
					</div>
				<?}?>
		<?}else{?>
			<div class="alert alert-info">Комментариев пока еще нет</div>
		<?}?>
	</div>

	<script type="text/ng-template" id="/comments.html">
		<div class="panel" ng-if="comments.length" ng-repeat="comment in comments" style="margin-left:{{comment.level * 50}}px;">
			<a name="comment{{comment.id}}"></a>
			<div class="panel-body">
				<div class="panel-title pull-left">
					#{{comment.id}} <strong>{{comment.userName}}</strong>
					<small class="text-muted">{{comment.date}}</small>
					<a href="" class="fa fa-trash" ng-if="comment.isMyComment && !comment.childsNum" ng-click="delete($event, comment);"></a>
				</div>

				<a href="" class="btn btn-xs btn-themes btn-default pull-right" ng-click="answer(comment.id)">Ответить</a>

				<div class="clearfix"></div>

				<p ng-bind-html="comment.text"></p>
			</div>
		</div>

		<div class="alert alert-info" ng-if="!comments.length">Комментариев пока еще нет</div>
	</script>

	<a name="addComment" id="addComment"></a>
	<h2>
		<span ng-hide="comment.parentId">Добавить комментарий</span>
		<span class="hide" ng-class="{hide:!comment.parentId}">Ответ на комментарий #{{answerComment.id}} &laquo;{{answerComment.userName}}&raquo;</span>
	</h2>
	<form method="POST" action="#addComment" name="commentsForm" novalidate ng-submit="addComment($event);">
		<div class="alert alert-success hide" ng-class="{hide: !commentsForm.successText}">{{commentsForm.successText}}</div>
		<div class="alert alert-danger hide" ng-class="{hide: !commentsForm.errorText}">{{commentsForm.errorText}}</div>

		<?if(!$USER->IsAuthorized()){?>
			<div class="form-group" ng-class="{'has-error': commentsForm.userName.$invalid && !commentsForm.$pristine}">
				<label for="commentName">ФИО</label>
				<input type="text" name="userName" class="form-control bg-white" id="commentName" ng-model="comment.userName" required>
			</div>
		<?}?>

		<div class="form-group" ng-class="{'has-error': commentsForm.text.$invalid && !commentsForm.$pristine}">
			<label for="commentText">Текст комментария</label>
			<textarea name="text" class="form-control" id="commentText" ng-model="comment.text" required></textarea>
		</div>

		<div class="form-group">
			<input type="hidden" value="<?=$_REQUEST['parentId'];?>" name="parentId" ng-model="comment.parentId">
			<input type="submit" name="send" class="btn btn-themes btn-default" value="Отправить">
		</div>
	</form>
</div>
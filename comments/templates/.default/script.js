// Отделяем пространство имен
(function(angular){
	"use strict";

	// Инициализация приложения, подключение модулей
	let pmApp = angular.module('pmComments',['ngResource', 'ngSanitize']);

	// Ресурс rest api
	pmApp.factory('commentRes', ['$resource', function($resource) {
		return $resource(pmAjaxPath);
	}]);

	// Контроллер комментариев
	pmApp.controller('CommentsCtrl', ['$scope', '$anchorScroll', '$timeout', 'commentRes', function($scope, $anchorScroll, $timeout, commentRes) {
		$scope.settings = {};
		$scope.comment = {};
		$scope.comments = [];

		// Инициализация параметров
		$scope.init = function(settings, comments){
			$scope.settings = settings;
			$scope.comments = comments;
		}

		// Ответ на комментарий
		$scope.answer = function(parentId){
			$scope.commentsForm.successText = '';

			// Устанавливаем parentId в форме
			$scope.comment.parentId = parentId;
			$scope.comments.forEach(function(comment){
				if(comment.id == parentId){
					$scope.answerComment = comment;
				}
			});

			// Скрол до формы
			$anchorScroll('addComment');
		}

		// Удаление комментария
		$scope.delete = function($event, delComment){
			// Если метод вызывается из формы
			if($event){
				$event.preventDefault();
				$event.stopPropagation();
			}

			// Удаляем комментарий из списка
			$scope.comments = $scope.comments.filter(function(comment){
				if(comment.id == delComment.parentId){
					comment.childsNum--;
				}
				return comment.id != delComment.id;
			});

			// Удаляем комментарий из БД
			let data = {
				url: $scope.settings.ajaxPath,
				id: delComment.id
			};
			angular.extend(data, $scope.settings);
			commentRes.delete(data);
		}

		// Добавление комментария
		$scope.addComment = function($event){
			$scope.commentsForm.$pristine = false;
			$scope.commentsForm.successText = '';
			$scope.commentsForm.errorText = '';
			let serverErrorText = 'В процессе добавления комментария произошла ошибка. Попробуйте еще раз.';
			let fieldText = 'Заполните все обязательные поля';

			// Если метод вызывается из формы
			if($event){
				$event.preventDefault();
				$event.stopPropagation();
			}

			// Форма валидна
			if($scope.commentsForm.$valid){
				let data = angular.copy($scope.comment)
				angular.extend(data, $scope.settings);

				// Сохраняем
				commentRes.save({url: $scope.settings.ajaxPath}, data).$promise.then(function(resp){
					// Ошибок нет
					if(resp.SUCCESS){
						$scope.commentsForm.successText = resp.SUCCESS;
						$scope.commentsForm.$pristine = true;
						$scope.comment.userName = $scope.comment.text = $scope.comment.parentId = '';

						//Добавляем комментарий в середину списка (к родителю)
						if(resp.DATA.parentId){
							let start = -1;
							let end = -1;
							let level = 0;
							$scope.comments.forEach(function(comment, indx){
								if(comment.id == resp.DATA.parentId){
									start = indx;
									level = comment.level + 1;
									comment.childsNum++;
								}

								if(comment.id != resp.DATA.parentId && start >= 0 && end < 0){
									end = indx;
								}
							});
							
							// Добавляем комментарий в массив
							resp.DATA.level = level;
							if(end < 0){
								$scope.comments.push(resp.DATA);;
							}else{
								$scope.comments.splice(end, 0, resp.DATA);
							}

							// Скролл до нового комментария после готовности DOM
							$timeout(function() {
								$anchorScroll('comment' + resp.DATA.id);
							})
						}else{
							// Добавляем комментарий в конец
							resp.DATA.level = 0;
							$scope.comments.push(resp.DATA);
						}

					}else if(resp.ERRORS && resp.ERRORS.length){
						// Ошибки есть
						$scope.commentsForm.errorText = join(resp.ERRORS, '<br /');
					}else{
						// Ошибки есть
						$scope.commentsForm.errorText = serverErrorText;
					}
				}, function(resp){
					$scope.commentsForm.errorText = serverErrorText;
				});
			}else{
				// Валидация не прошла
				$scope.commentsForm.errorText = fieldText;
			}
		}
	}]);
})(window.angular)

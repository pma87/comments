<?php
	if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

	use Bitrix\Highloadblock as HL;
	use Bitrix\Main\Entity;
	use Bitrix\Main\Type\DateTime;

	class Comments extends CBitrixComponent {
		private $_request = array();

		public function __construct($component=null) {
			parent::__construct($component);
			$this->_request = $this->getRequestData();

			// Эмуляция Full Rest api без javascript
			if ($this->_request['method']) {
				$this->action = $this->_request['method'];
			} else {
				$this->action = strtolower($_SERVER['REQUEST_METHOD']);
			}
		}

		private function getRequestData() {
			$data = json_decode(file_get_contents('php://input'), true);

			if (empty($data)) {
				$data = $_REQUEST;
			}
			return $data;
		}

		// Подготовка параметров
		public function onPrepareComponentParams($arParams) {
			global $APPLICATION;

			if (!strlen($arParams['OBJECT_TYPE'])) {
				$arParams['OBJECT_TYPE'] = 'PAGE';
			}

			if (!strlen($arParams['OBJECT'])) {
				$arParams['OBJECT'] = $APPLICATION->GetCurPage(true);
			}

			return $arParams;
		}

		// Выполнение компонента
		public function executeComponent() {
			global $USER;

			// Подключение модулей
			if (CModule::IncludeModule('iblock') && CModule::IncludeModule('highloadblock')) {
				// Вызов метода действия
				if (method_exists($this, $this->action)) {
					$res = call_user_func(array($this, $this->action));
					$this->showResult($res);
				}
			}
		}

		// Добавление комментария
		private function post() {
			global $USER;
			$arResult = array(
				'DATA' => array(),
				'SUCCESS' => false,
				'ERRORS' => array(),
			);

			// Данные комментария
			$commentData = array(
				'UF_OBJECT' => $this->getObjectId(),
				'UF_PARENT_ID' => $this->_request['parentId'],
				'UF_DATE' => new DateTime(),
				'UF_SITE_ID' => SITE_ID,
			);

			// Пользователь авторизован
			if ($USER->IsAuthorized()) {
				$commentData['UF_USER_ID'] = $USER->GetID();
				$commentData['UF_USER_NAME'] = $USER->GetFullName();

				// Если имя у пользователя не задано
				if (!$commentData['UF_USER_NAME']) {
					$commentData['UF_USER_NAME'] = $USER->GetLogin();
				}
			} elseif ($this->_request['userName']) {
				$commentData['UF_USER_NAME'] = $this->_request['userName'];
			} else {
				$arResult['ERRORS']['userName'] = 'Авторизуйтесь или заполните поле "ФИО"';
			}

			// Текст комментария
			if ($this->_request['text']) {
				$commentData['UF_TEXT'] = $this->_request['text'];
			} else {
				$arResult['ERRORS']['text'] = 'Поле "Текст" обязательно для заполнения111';
			}

			// Ошибок нет - добавляем комментарий
			if (!count($arResult['ERRORS'])) {
				$hlblock = HL\HighloadBlockTable::getById($this->arParams['HL_ID'])->fetch();
				$entity = HL\HighloadBlockTable::compileEntity($hlblock);
				$entity_data_class = $entity->getDataClass();

				$result = $entity_data_class::add($commentData);

				// Комментарий добавлен успешно
				if ($result->isSuccess()) {
					$arResult['DATA'] = array(
						'id' => $result->getId(),
						'parentId' => $commentData['UF_PARENT_ID'],
						'date' => $commentData['UF_DATE']->toString(),
						'userName' => $commentData['UF_USER_NAME'],
						'text' => $commentData['UF_TEXT'],
						'isMyComment' => $commentData['UF_USER_ID'] ? true : false,
						'childsNum' => false,
					);
					$arResult['SUCCESS'] = 'Комментарий успешно добавлен';
				} else {
					// Ошибки при добавлении
					$arResult['ERRORS'] = array_merge($arResult['ERRORS'], $result->getErrors());
				}
			}

			// Если не ajax запрос, то страница перезагружается и нужно показать комментарии
			if (!$this->isAjax()) {
				$arResult = $this->get();
			}

			return $arResult;
		}

		// Список комментариев
		private function get($parentId = false, $level = -1) {
			global $USER;
			if ($USER->IsAuthorized()) {
				$userId = $USER->GetID();
			} else {
				$userId = false;
			}

			$arResult = array(
				'DATA' => array(),
				'SUCCESS' => false,
				'ERRORS' => array(),
			);

			if (!$parentId) {
				$parentId = $this->_request['filter']['parentId'];
			}

			$level++;

			$hlblock = HL\HighloadBlockTable::getById($this->arParams['HL_ID'])->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock);
			$entity_data_class = $entity->getDataClass();

			$filter = array(
				'UF_OBJECT' => $this->getObjectId(),
				'UF_SITE_ID' => SITE_ID,
			);

			$filter['UF_PARENT_ID'] = $parentId;

			// Получаем список комментариев
			$rs = $entity_data_class::getList(array(
				'filter' => $filter,
				'order' => array(
					'UF_DATE' => 'ASC'
				),
			));

			$parentIdArr = array();
			while ($row = $rs->fetch()) {
				$parentIdArr[$row['ID']] = $row['ID'];

				$childs = $this->get($row['ID'], $level);
				$arResult['DATA'][] = array(
					'id' => $row['ID'],
					'level' => $level,
					'parentId' => $row['UF_PARENT_ID'],
					'date' => $row['UF_DATE']->toString(),
					'userName' => htmlspecialcharsBx($row['UF_USER_NAME']),
					'text' => nl2br(htmlspecialcharsBx($row['UF_TEXT'])),
					'isMyComment' => ($userId && $userId == $row['UF_USER_ID']),
					'childsNum' => count($childs['DATA']),
				);

				$arResult['DATA'] = array_merge_recursive($arResult['DATA'], $childs['DATA']);
			}

			// Проверяем вложенные комментарии

			return $arResult;
		}

		// Удаление комментария
		private function delete() {
			global $USER;

			// Удаление только авторизованным пользователем
			if ($this->_request['id'] && $USER->IsAuthorized()) {
				$hlblock = HL\HighloadBlockTable::getById($this->arParams['HL_ID'])->fetch();
				$entity = HL\HighloadBlockTable::compileEntity($hlblock);
				$entity_data_class = $entity->getDataClass();

				// Проверяем соответствие пользователя
				$rs = $entity_data_class::GetById($this->_request['id']);
				if ($row = $rs->fetch()) {
					if ($row['UF_USER_ID'] == $USER->GetID()) {
						$entity_data_class::Delete($this->_request['id']);
					}
				}
			}

			// Если не ajax запрос, то страница перезагружается и нужно показать комментарии
			if (!$this->isAjax()) {
				$arResult = $this->get();
			}

			return $arResult;
		}

		// Идентификатор объекта комментирования
		private function getObjectId() {
			return $this->arParams['OBJECT_TYPE'] . '-' . $this->arParams['OBJECT'];
		}

		// Определение типа запроса
		private function isAjax() {
			if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' || $this->arParams['IS_AJAX']) {
				$res = true;
			} else {
				$res = false;
			}

			return $res;
		}

		// Путь до AJAX файла
		private function getAjaxPath() {
			$path = substr(__DIR__, strlen($_SERVER['DOCUMENT_ROOT'])) . '/ajax.php';
			return $path;
		}

		// Вывод результата в необходимом формате (html, json, ...)
		private function showResult($res) {
			$this->arResult = $res;

			// Json
			if (strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'json')) {
				header('Content-Type: application/json');
				echo json_encode($this->arResult);
			} else {
				// HTML
				$this->arResult['AJAX_PATH'] = $this->getAjaxPath();
				$this -> includeComponentTemplate();
			}
		}
	}
?>
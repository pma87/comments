<?php
	define('STOP_STATISTICS', true);
	global $APPLICATION;

	require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
	$request = json_decode(file_get_contents('php://input'), true);

	if(!$request){
		$request = $_REQUEST;
	}
?>
<?$APPLICATION->IncludeComponent(
	"pm:comments",
	"",
	Array(
		"OBJECT" => $request['commentObject'],
		"OBJECT_TYPE" => $request['commentObjectType'],
		"HL_ID" => $request['commentHlId'],
		"IS_AJAX" => true,
	)
);?>
<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
?>